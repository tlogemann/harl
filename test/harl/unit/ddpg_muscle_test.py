from unittest import TestCase
from palaestrai.agent import ActuatorInformation
from palaestrai.types import Box

from harl.ddpg.muscle import DDPGMuscle


class TestDDPGMuscle(TestCase):
    def test_output_scaling(self):
        muscle = DDPGMuscle("Test", "Test", 1, 2)
        actuators_available = [
            ActuatorInformation(
                0,
                actuator_id=0,
                action_space=Box(low=0.0, high=3.0, shape=(1,)),
            ),
            ActuatorInformation(
                0,
                actuator_id=0,
                action_space=Box(low=0.0, high=10.0, shape=(1,)),
            ),
        ]
        actions = [0.5, 0.5]

        output = muscle.output_scaling(actuators_available, actions)

        self.assertEqual(output[0].setpoint, 1.5)
        self.assertEqual(output[1].setpoint, 5)

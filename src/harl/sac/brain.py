import socket
from copy import deepcopy
from importlib import import_module
from pathlib import Path
from typing import Union, List

import itertools
import numpy as np
import torch as T
from torch.optim import Adam

from harl.Networks.replaybuffer import ReplayBuffer
from harl.sac.network import Actor, Critic
from palaestrai.agent import (
    Brain,
    SensorInformation,
    ActuatorInformation,
    Objective,
    LOG,
)
from palaestrai.core.protocol import MuscleUpdateResponse
from palaestrai.types import Box
from palaestrai.util.exception import NoneInputError


def load_module(path: str):
    module, clazz = path.rsplit(".", 1)
    module = import_module(module)
    return getattr(module, clazz)


class SACBrain(Brain):
    def __init__(
        self,
        muscle_updates_listen_uri_or_socket: Union[str, socket.socket],
        sensors: List[SensorInformation],
        actuators: List[ActuatorInformation],
        objective: Objective,
        seed: int,
        **params,
    ):
        super().__init__(
            muscle_updates_listen_uri_or_socket,
            sensors,
            actuators,
            objective,
            seed,
            **params,
        )
        assert len(sensors) != 0
        assert len(actuators) != 0
        self.act_limit = None
        for act in actuators:
            space = act.action_space
            if isinstance(space, Box):
                if self.act_limit is not None:
                    if self.act_limit < space.high[0]:
                        self.act_limit = space.high[0]
                else:
                    self.act_limit = space.high[0]
            else:
                LOG.error(
                    "Brain(id=0x%x) has received a wrong action space %s. Requires a Box action space.",
                    id(self),
                    type(space),
                )
                raise TypeError

        self.replay_size = params.get("replay_size", int(1e6))
        self.gamma = params.get("gamma", 0.99)
        self.polyak = params.get("polyak", 0.995)
        self.lr = params.get("lr", 1e-3)
        self.alpha = params.get("alpha", 0.2)
        self.batch_size = params.get("batch_size", 100)
        self.start_steps = params.get("start_steps", 10000)
        self.update_after = params.get("update_after", 1000)
        self.update_every = params.get("update_every", 50)

        self.activation = load_module(
            params.get("activation", "torch.nn.ReLU")
        )
        self.hidden_size = params.get("hidden_size", (256, 256))
        self.n_sensors = len(sensors)
        self.n_actuators = len(actuators)

        self.device = T.device("cuda:0" if T.cuda.is_available() else "cpu")

        self.actor = Actor(
            self.n_sensors, self.n_actuators, self.act_limit
        ).to(self.device)
        self.critic = Critic(self.n_sensors, self.n_actuators).to(self.device)
        self.actor_target = deepcopy(self.actor)
        self.critic_target = deepcopy(self.critic)
        for p in self.actor_target.parameters():
            p.requires_grad = False
        for p in self.critic_target.parameters():
            p.requires_grad = False

        self.pi_optimizer = Adam(self.actor.pi.parameters(), lr=self.lr)
        self.q_params = itertools.chain(
            self.critic.q1.parameters(), self.critic.q2.parameters()
        )
        self.q_optimizer = Adam(self.q_params, lr=self.lr)

        self.replay_buffer = ReplayBuffer(
            state_dim=self.n_sensors,
            action_dim=self.n_actuators,
            max_size=self.replay_size,
            device=self.device,
        )

        T.manual_seed(
            seed
        )  # siehe https://pytorch.org/docs/stable/notes/randomness.html
        self.init_muscle = False
        self.step = 0

    def thinking(
        self,
        muscle_id,
        readings,
        actions,
        reward,
        next_state,
        done,
        additional_data,
    ) -> MuscleUpdateResponse:

        if not self.init_muscle:
            self.init_muscle = True
            return MuscleUpdateResponse(True, self._get_init_dict())
        if (
            readings is None
            or actions is None
            or reward is None
            or not next_state
            or done is None
        ):
            LOG.error(
                "Brain(id=0x%x) " "has received a none value",
                id(self),
            )
            raise NoneInputError

        self._remember(readings, actions, reward, next_state, done)
        if (
            self.step >= self.update_after
            and self.step % self.update_every == 0
        ):
            return self.update()
        else:
            return MuscleUpdateResponse(False, None)

    def compute_loss_q(self, data):
        o, a, r, o2, d = (
            data["obs"],
            data["act"],
            data["rew"],
            data["obs2"],
            data["done"],
        )

        q1 = self.critic.q1(o, a)
        q2 = self.critic.q2(o, a)

        # Bellman backup for Q functions
        with T.no_grad():
            # Target actions come from *current* policy
            a2, logp_a2 = self.actor.pi(o2)

            # Target Q-values
            q1_pi_targ = self.critic_target.q1(o2, a2)
            q2_pi_targ = self.critic_target.q2(o2, a2)
            q_pi_targ = T.min(q1_pi_targ, q2_pi_targ)
            backup = r + self.gamma * (1 - d) * (
                q_pi_targ - self.alpha * logp_a2
            )

        # MSE loss against Bellman backup
        loss_q1 = ((q1 - backup) ** 2).mean()
        loss_q2 = ((q2 - backup) ** 2).mean()
        loss_q = loss_q1 + loss_q2

        # Useful info for logging
        q_info = dict(Q1Vals=q1.detach().numpy(), Q2Vals=q2.detach().numpy())

        return loss_q, q_info

    def compute_loss_pi(self, data):
        o = data["obs"]
        pi, logp_pi = self.actor.pi(o)
        q1_pi = self.critic.q1(o, pi)
        q2_pi = self.critic.q2(o, pi)
        q_pi = T.min(q1_pi, q2_pi)

        # Entropy-regularized policy loss
        loss_pi = (self.alpha * logp_pi - q_pi).mean()

        # Useful info for logging
        pi_info = dict(LogPi=logp_pi.detach().numpy())

        return loss_pi, pi_info

    def update(self):
        for j in range(self.update_every):
            data = self.replay_buffer.sample(self.batch_size)
            self.q_optimizer.zero_grad()
            loss_q, q_info = self.compute_loss_q(data)
            loss_q.backward()
            self.q_optimizer.step()

            for p in self.q_params:
                p.requires_grad = False

            # Next run one gradient descent step for pi.
            self.pi_optimizer.zero_grad()
            loss_pi, pi_info = self.compute_loss_pi(data)
            loss_pi.backward()
            self.pi_optimizer.step()

            for p in self.q_params:
                p.requires_grad = True

            # updated target net
            with T.no_grad():
                for p, p_targ in zip(
                    self.actor.parameters(), self.actor_target.parameters()
                ):
                    p_targ.data.mul_(self.polyak)
                    p_targ.data.add_((1 - self.polyak) * p.data)

                for p, p_targ in zip(
                    self.critic.parameters(), self.critic_target.parameters()
                ):
                    p_targ.data.mul_(self.polyak)
                    p_targ.data.add_((1 - self.polyak) * p.data)

        return MuscleUpdateResponse(True, self._get_actor_params())

    def store_model(self, path):
        try:
            Path(path).mkdir(parents=True, exist_ok=True)
        except OSError as e:
            LOG.warning(
                "Brain(id=0x%x) failed to create path to save model %s",
                id(self),
                str(e),
            )
            return

        T.save(self.actor, f"{path}/sac_actor")
        T.save(self.actor_target, f"{path}/sac_actor_target")
        T.save(self.critic, f"{path}/sac_critic")
        T.save(self.critic_target, f"{path}/sac_critic_target")

    def load_model(self, path):
        self.actor = T.load(f"{path}/sac_actor")
        self.actor_target = T.load(f"{path}/sac_actor_target")
        self.critic = T.load(f"{path}/sac_critic")
        self.critic_target = T.load(f"{path}/sac_critic_target")

    def _get_init_dict(self):
        return {
            "weights": dict(self._get_actor_params()),
            "n_inputs": self.n_sensors,
            "n_outputs": self.n_actuators,
            "act_limit": self.act_limit,
        }

    def _remember(self, readings, actions, reward, next_state, done):
        self.replay_buffer.add(
            state=np.array(readings),
            action=np.array(actions),
            reward=reward,
            next_state=np.concatenate([s() for s in next_state]),
            done=done,
        )

    def _get_actor_params(self):
        return dict(self.actor.named_parameters())

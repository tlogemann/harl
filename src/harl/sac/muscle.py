import numpy as np
import torch as T

from harl.sac.network import Actor
from palaestrai.agent import Muscle, LOG
from palaestrai.types import Box
from palaestrai.types.mode import Mode


def output_scaling(actuators_available, actions):
    """Method to scale and crop the output to the given actuator space.

    If the network output space changes, this method needs to be
    modified as well.
    """

    input_range = [-1, 1]

    for idx, action in enumerate(actions):
        assert isinstance(
            actuators_available[idx].action_space, Box
        ), f'{actuators_available[idx].action_space} must be "Box" type'
        aspace = actuators_available[idx].action_space
        value = np.interp(action, input_range, [aspace.low[0], aspace.high[0]])

        try:
            actuators_available[idx](np.array([value]).astype(aspace.dtype))
        except Exception as err:
            LOG.warning(
                "OutOfActionSpaceError for actuator %s:"
                "value %s of type %s (converted to %s) is not contained "
                "in %s. Setting this value anyways but beware that "
                "behavior might be unexpected!",
                actuators_available[idx].actuator_id,
                str(value),
                type(value),
                np.array([value]).astype(aspace.dtype),
                aspace
            )
            actuators_available[idx]._setpoint = np.array([value]).astype(aspace.dtype)
    return actuators_available


class SACMuscle(Muscle):
    def __init__(self, broker_uri, brain_uri, uid, brain_id):
        super().__init__(broker_uri, brain_uri, uid, brain_id)
        self.model = None

    def setup(self, *args, **kwargs):
        pass

    def propose_actions(
        self, sensors, actuators_available, is_terminal=False
    ) -> tuple:
        values = np.concatenate([val() for val in sensors])
        obs = T.tensor(
            values,
            dtype=T.float,
        )
        state = obs.to(T.device("cpu"))
        if self._mode == Mode.TRAIN:
            actions = self.model.act(state, False)[0]
        else:
            # In test mode we use det
            actions = self.model.act(state, True)[0]
        env_actuators = output_scaling(actuators_available, actions)

        return env_actuators, actions, values, {}

    def update(self, update):
        if self.model is None and update is not None:
            self._init(**update)
        elif update is not None:
            self._update_weights(weights=update)
        elif self.model is None and update is None:
            LOG.error(
                "Muscle(id=0x%x) " "Update and Model are None!", id(self)
            )

    def prepare_model(self):
        data = open(self._load_path + "/sac_actor", "rb")
        self.model = T.load(data).to("cpu")

    def __repr__(self):
        pass

    def _init(self, n_inputs, n_outputs, weights, act_limit):
        """Initialize the actor-network."""
        self.model = Actor(
            observation_space=n_inputs,
            action_space=n_outputs,
            act_limit=act_limit,
        )
        self._update_weights(weights=weights)

    def _update_weights(self, weights):
        self.model.load_state_dict(weights)

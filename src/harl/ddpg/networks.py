# networks.py

"""
Actor and Critic pytorch-class for standard DDPG implementation in hARLequin.

"""

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim


class CriticNetwork(nn.Module):
    def __init__(self, beta, n_inputs, fc_dims, n_actions, name):
        super(CriticNetwork, self).__init__()
        self.n_inputs = n_inputs
        self.fc_dims = fc_dims
        self.n_actions = n_actions
        self.name = name

        fcs = [nn.Linear(self.n_inputs, self.fc_dims[0])]
        fcs += [
            nn.Linear(self.fc_dims[i], self.fc_dims[i + 1])
            for i in range(len(self.fc_dims) - 1)
        ]
        self.fcs = nn.ModuleList(fcs)

        self.bns = nn.ModuleList(
            [nn.LayerNorm(n_neurons) for n_neurons in self.fc_dims]
        )
        # self.bns = nn.ModuleList([nn.BatchNorm1d(n_neurons) for n_neurons in self.fc_dims]) # TODO: make choosable

        self.action_value = nn.Linear(self.n_actions, self.fc_dims[-1])

        # Output layer
        self.q = nn.Linear(self.fc_dims[-1], 1)

        # Initialize weights
        for idx, fc in enumerate(self.fcs):
            f = 1.0 / np.sqrt(fc.weight.data.size()[0])
            self.fcs[idx].weight.data.uniform_(-f, f)
            self.fcs[idx].bias.data.uniform_(-f, f)

        f3 = 0.003
        self.q.weight.data.uniform_(-f3, f3)
        self.q.bias.data.uniform_(-f3, f3)

        f4 = 1.0 / np.sqrt(self.action_value.weight.data.size()[0])
        self.action_value.weight.data.uniform_(-f4, f4)
        self.action_value.bias.data.uniform_(-f4, f4)

        self.optimizer = optim.Adam(
            self.parameters(), lr=beta, weight_decay=0.01
        )

        self.device = torch.device(
            "cuda:0" if torch.cuda.is_available() else "cpu"
        )

        self.to(self.device)

    def forward(self, state, action):
        state = state.view(-1, self.n_inputs)
        for fc, bn in zip(self.fcs[:-1], self.bns[:-1]):
            state = fc(state)
            state = bn(state)
            state = F.relu(state)

        state = self.fcs[-1](state)
        state = self.bns[-1](state)
        action_value = self.action_value(action)
        state_action_value = F.relu(torch.add(state, action_value))
        # TODO: Double relu?
        # state_action_value = torch.add(state_value, action_value)
        state_action_value = self.q(state_action_value)

        return state_action_value


class ActorNetwork(nn.Module):
    def __init__(
        self,
        n_inputs: int,
        fc_dims: list,
        n_actions: int,
        name: str,
        alpha=0.0001,
    ):
        super(ActorNetwork, self).__init__()
        self.n_inputs = n_inputs
        self.fc_dims = fc_dims
        self.n_actions = n_actions
        self.name = name
#        fcs = [nn.Linear(self.n_inputs, self.fc_dims[0])]
#        fcs += [
#            nn.Linear(self.fc_dims[i], self.fc_dims[i + 1])
#            for i in range(len(self.fc_dims) - 1)
#        ]
#        self.fcs = nn.ModuleList(fcs)
#
#        self.bns = nn.ModuleList(
#            [nn.LayerNorm(n_neurons) for n_neurons in self.fc_dims]
#        )
#
#        self.mu = nn.Linear(self.fc_dims[-1], self.n_actions)
#
#        for idx, fc in enumerate(self.fcs):
#            f = 1.0 / np.sqrt(fc.weight.data.size()[0])
#            self.fcs[idx].weight.data.uniform_(-f, f)
#            self.fcs[idx].bias.data.uniform_(-f, f)
#
#        f3 = 0.003
#        self.mu.weight.data.uniform_(-f3, f3)
#        self.mu.bias.data.uniform_(-f3, f3)
#
#        self.optimizer = optim.Adam(self.parameters(), lr=alpha)
#        self.device = torch.device(
#            "cuda:0" if torch.cuda.is_available() else "cpu"
#        )
#
#        self.to(self.device)

        self.nn_model = nn.Sequential(
            nn.Linear(self.n_inputs, 8), nn.ReLU(),
            nn.Linear(8, 8), nn.ReLU(),
            nn.Linear(8, self.n_actions)
        )

        self.optimizer = optim.Adam(self.nn_model.parameters(), lr=alpha)

        self.device = torch.device(
            "cuda" if torch.cuda.is_available() else "cpu"
        )
        self.nn_model.to(self.device)


    def forward(self, state):
        output = state.view(-1, self.n_inputs)
#        for fc, bn in zip(self.fcs, self.bns):
#            output = fc(output)
#            output = bn(output)
#            output = F.relu(output)
#
#        output = self.mu(output)
#        output = torch.sigmoid(output)

        output = self.nn_model(output)

        return output

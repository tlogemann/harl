import math

from palaestrai.agent import RewardInformation
from palaestrai.agent.objective import Objective
from typing import List


def gaus_norm(
    raw_value: float,
    mu: float = 1,
    sigma: float = 0.1,
    c: float = 0.5,
    a: float = -1,
):
    gaus_reward = a * math.exp(-((raw_value - mu) ** 2) / (2 * sigma**2)) - c
    return gaus_reward


class DummyObjective(Objective):
    def internal_reward(self, rewards: List["RewardInformation"]) -> float:
        reward = 0
        for rwd in rewards:
            reward = rwd.reward_value
        return reward


class IABObjective(Objective):
    def __init__(self, params):
        super().__init__(params)

    def internal_reward(self, rewards: List[RewardInformation]):
        reward = [0]
        if self.params["is_defender"]:
            for rwd in rewards:
                reward.append(gaus_norm(rwd.reward_value, 1, 0.02, 1.0, 2))
            return sum(reward) / len(reward)
        else:
            for rwd in rewards:
                reward.append(
                    gaus_norm(rwd.reward_value, 1, 0.025, -1.0, -1.5)
                )
            return sum(reward) / len(reward)


class NoExtGridObjective(Objective):
    def __init__(self, params):
        super().__init__(params)

    def internal_reward(self, rewards: List[RewardInformation]):
        reward = 0.0
        if self.params["is_defender"]:
            for rwd in rewards:
                if rwd.reward_id == "grid_health_reward":
                    reward += gaus_norm(rwd.reward_value, 1, 0.02, 1.0, 2)
                elif rwd.reward_id == "external_grid_penalty_reward":
                    reward += (rwd.reward_value / 10) * -1
            if reward is not None:
                return reward
            else:
                return 0.0
        else:
            for rwd in rewards:
                if rwd.reward_id == "grid_health_reward":
                    reward += gaus_norm(rwd.reward_value, 1, 0.025, -1.0, -1.5)
                elif rwd.reward_id == "external_grid_penalty_reward":
                    reward += (rwd.reward_value / 10) * -1
            if reward is not None:
                return reward
            else:
                return 0.0

# brain.py
"""
Brain for the DDPG standard implementation in palaestrai.

"""
import logging
import socket
import uuid
from pathlib import Path
from typing import Union, List

import numpy as np
import torch as T
import torch.nn.functional as F

from palaestrai.agent import SensorInformation, ActuatorInformation, Objective
from palaestrai.agent.brain import Brain
from palaestrai.core.protocol.muscle_update_rsp import MuscleUpdateResponse
from palaestrai.util.exception import NoneInputError
from .buffer import ReplayBuffer
from .networks import ActorNetwork, CriticNetwork

LOG = logging.getLogger(__name__)


def get_load_path(base_path: str, i: int):
    folders = sorted([p for p in Path(base_path).iterdir() if p.is_dir()])
    return folders[i]


class DDPGBrain(Brain):
    def __init__(
        self,
        muscle_updates_listen_uri_or_socket: Union[str, socket.socket],
        sensors: List[SensorInformation],
        actuators: List[ActuatorInformation],
        objective: Objective,
        seed: int,
        **params,
    ):
        super().__init__(
            muscle_updates_listen_uri_or_socket,
            sensors,
            actuators,
            objective,
            seed,
            **params,
        )

        assert len(sensors) != 0
        assert len(actuators) != 0
        T.manual_seed(seed)
        self.gamma = params["gamma"]
        self.tau = params["tau"]
        self.batch_size = params["batch_size"]
        self.alpha = params["alpha"]
        self.beta = params["beta"]
        self.fc_dims = params["fc_dims"]
        self.n_actions = len(actuators)
        self.n_sensors = len(sensors)
        self.log_reward = []  # TODO: delete
        self.avrg_reward = []
        self.critic_error = []
        self.actor_error = []
        self.init_muscle = False
        self.id = uuid.uuid4()
        self.device = T.device(
            "cuda" if T.cuda.is_available() else "cpu"
        )

        self.memory = ReplayBuffer(
            params["replay_size"], self.n_sensors, self.n_actions
        )

        self.actor = ActorNetwork(
            self.n_sensors,
            fc_dims=self.fc_dims,
            n_actions=self.n_actions,
            name="actor",
            alpha=self.alpha,
        )

        self.critic = CriticNetwork(
            self.beta,
            self.n_sensors,
            fc_dims=self.fc_dims,
            n_actions=self.n_actions,
            name="critic",
        )

        self.target_actor = ActorNetwork(
            self.n_sensors,
            fc_dims=self.fc_dims,
            n_actions=self.n_actions,
            name="target_actor",
            alpha=self.alpha,
        )

        self.target_critic = CriticNetwork(
            self.beta,
            self.n_sensors,
            fc_dims=self.fc_dims,
            n_actions=self.n_actions,
            name="target_critic",
        )

        self._equalize_target_nets()

    def load_model(self, path):

        self.critic = T.load(f"{path}/ddpg_critic", map_location=self.device)
        self.critic.device = self.device

        self.actor = T.load(f"{path}/ddpg_actor", map_location=self.device)
        self.actor.device = self.device

        self.target_critic = T.load(f"{path}/ddpg_target_critic", map_location=self.device)
        self.target_critic.device = self.device

        self.target_actor = T.load(f"{path}/ddpg_target_actor", map_location=self.device)
        self.target_actor.device = self.device

    def store_model(self, path):
        self.store_path = path
        self._store_model_to_subfolder(path)

    def _store_model_to_subfolder(self, path):
        try:
            Path(path).mkdir(parents=True, exist_ok=True)
        except OSError as e:
            LOG.warning(
                "Brain(id=0x%x) failed to create path to save model %s",
                id(self),
                str(e),
            )
            return

        T.save(self.critic, f"{path}/ddpg_critic")
        T.save(self.actor, f"{path}/ddpg_actor")
        T.save(self.actor.nn_model, f"{path}/ddpg_actor_nn")
        T.save(
            self.target_actor,
            f"{path}/ddpg_target_actor",
        )
        T.save(
            self.target_critic,
            f"{path}/ddpg_target_critic",
        )

    def thinking(
        self,
        muscle_id,
        readings,
        actions,
        reward,
        next_state,
        done,
        additional_data,
    ):
        self.log_reward.append(reward)
        self.avrg_reward.append(np.mean(self.log_reward[-300:]))

        if not self.init_muscle:
            self.init_muscle = True
            response = MuscleUpdateResponse(True, self._get_init_dict())
        else:
            if done:
                response = MuscleUpdateResponse(False, None)
            else:
                if (
                    readings is None
                    or actions is None
                    or reward is None
                    or not next_state
                    or done is None
                ):
                    LOG.error(
                        "Brain(id=0x%x) " "has received a none value",
                        id(self),
                    )
                    raise NoneInputError
                else:
                    self._remember(readings, actions, reward, next_state, done)
                    # Currently training and updating every step!
                    response = self._learn()

        return response

    def _remember(self, state, action, reward, next_state, done):
        self.memory.store_transition(
            state=np.array(state),
            action=np.array(action),
            reward=reward,
            state_=np.concatenate([s() for s in next_state]),
            done=done,
        )

    def _learn(self):
        """
        Draw mini batch from memory, calculate loss and backpropagate.
        """

        if self.memory.mem_cntr < self.batch_size:
            return MuscleUpdateResponse(True, None)

        states, next_states, actions, rewards, dones = self._create_batch()

        target_actions = self.target_actor.forward(next_states)
        critic_value_ = self.target_critic.forward(next_states, target_actions)
        critic_value = self.critic.forward(states, actions)

        critic_value_[dones] = 0.0
        critic_value_ = critic_value_.view(-1)

        target = rewards + self.gamma * critic_value_
        target = target.view(self.batch_size, 1)

        self.critic.optimizer.zero_grad()
        critic_loss = F.mse_loss(target, critic_value)
        self.critic_error.append(critic_loss.item())  # TODO: Delete
        critic_loss.backward()
        self.critic.optimizer.step()

        self.actor.optimizer.zero_grad()
        # Backpropagate and maximize the mean expected Q-value
        actor_loss = -self.critic.forward(states, self.actor.forward(states))
        actor_loss = T.mean(actor_loss)
        self.actor_error.append(actor_loss.item())
        actor_loss.backward()
        self.actor.optimizer.step()

        self._update_target_parameters(tau=self.tau)

        return MuscleUpdateResponse(True, self._get_actor_params())

    def _create_batch(self):
        batch = self.memory.sample_buffer(self.batch_size)
        states, actions, rewards, next_states, dones = batch

        states = T.tensor(states, dtype=T.float).to(self.actor.device)
        next_states = T.tensor(next_states, dtype=T.float).to(
            self.actor.device
        )
        actions = T.tensor(actions, dtype=T.float).to(self.actor.device)
        rewards = T.tensor(rewards, dtype=T.float).to(self.actor.device)
        dones = T.tensor(dones).to(self.actor.device).type(T.LongTensor)

        return states, next_states, actions, rewards, dones

    def _equalize_target_nets(self):
        """Set parameters of target nets equal to actor/critic net."""
        self._update_target_parameters(tau=1)

    def _update_target_parameters(self, tau):
        """Soft update of the target network parameters."""
        actor_params = dict(self.actor.named_parameters())
        critic_params = dict(self.critic.named_parameters())
        target_actor_params = dict(self.target_actor.named_parameters())
        target_critic_params = dict(self.target_critic.named_parameters())

        for name in critic_params:
            critic_params[name] = (
                tau * critic_params[name].clone()
                + (1 - tau) * target_critic_params[name].clone()
            )

        for name in actor_params:
            actor_params[name] = (
                tau * actor_params[name].clone()
                + (1 - tau) * target_actor_params[name].clone()
            )

        self.target_critic.load_state_dict(critic_params)
        self.target_actor.load_state_dict(actor_params)

    def _get_init_dict(self):
        """Special dict for the initialzation of the muscle."""
        return {
            "weights": self._get_actor_params(),
            "n_inputs": self.n_sensors,
            "n_outputs": self.n_actions,
            "fc_dims": self.fc_dims,
        }

    def _get_actor_params(self):
        return dict(self.actor.named_parameters())

# noise.py
"""
Noise class for DDPG actions to add exploration.
TODO: gaussian noise as good as OU but simpler! (https://spinningup.openai.com/en/latest/algorithms/ddpg.html)

"""
import logging

import numpy as np

LOG = logging.getLogger(__name__)


class OUActionNoise:
    def __init__(self, mu, sigma=0.15, theta=0.2, dt=1e-2, x0=None):
        self.theta = theta
        self.mu = mu
        self.sigma = sigma
        self.dt = dt
        self.x0 = x0
        self.reset()

    def __call__(self):
        x = (
                self.x_prev
                + self.theta * (self.mu - self.x_prev) * self.dt
                + self.sigma
                * np.sqrt(self.dt)
                * np.random.normal(size=self.mu.shape)
        )
        if np.isnan(x):
            x = self.x_prev
            LOG.warning("Noise was NaN")
        else:
            self.x_prev = x
        return x

    def reset(self):
        self.x_prev = (
            self.x0 if self.x0 is not None else np.zeros_like(self.mu)
        )


class GaussianNoise:
    def __init__(self, sigma=0.15):
        self.sigma = sigma

    def __call__(self):
        return [float(np.random.normal(0, self.sigma, 1))]

# muscle.py
"""
Muscle for the DDPG standard implementation in palaestrai.

"""
from typing import Optional

import numpy as np
import torch

from palaestrai.agent.muscle import Muscle
from palaestrai.types.box import Box
from .networks import ActorNetwork
from .noise import OUActionNoise, GaussianNoise
import logging
from palaestrai.types.mode import Mode

LOG = logging.getLogger(__name__)


class DDPGMuscle(Muscle):
    MAKE_NOISE = True
    NOISE_FUNC = 'Gaussian'
    SIGMA = 0.15
    THETA = 0.2
    DT = 0.01

    def __init__(self, broker_uri, brain_uri, uid, brain_id, make_noise, noise_func=None, sigma=None, theta=None,
                 dt=None, **params):
        super().__init__(broker_uri, brain_uri, uid, brain_id)
        self.model = None
        self.noise = None
        self.device = torch.device(
            "cuda" if torch.cuda.is_available() else "cpu"
        )
        self.make_noise = make_noise if not None else DDPGMuscle.MAKE_NOISE
        self.noise_func = noise_func if not None else DDPGMuscle.NOISE_FUNC
        self.sigma = sigma if not None else DDPGMuscle.SIGMA
        self.theta = theta if not None else DDPGMuscle.THETA
        self.dt = dt if not None else DDPGMuscle.DT
        # self.make_noise = params.get('make_noise', DDPGMuscle.MAKE_NOISE)
        # self.noise_func = params.get('noise_func', DDPGMuscle.NOISE_FUNC)
        # self.sigma = params.get('sigma', DDPGMuscle.SIGMA)
        # self.theta = params.get('theta', DDPGMuscle.THETA)
        # self.dt = params.get('dt', DDPGMuscle.DT)

    def setup(self):
        pass

    @staticmethod
    def output_scaling(actuators_available, actions):
        """Method to scale the output to the given actuator space.

        If the network output space changes, this method needs to be
        modified as well.
        """
        assert len(actions) == len(actuators_available)

        input_range = [0, 1]

        value_list = []

        for idx, action in enumerate(actions):
            assert isinstance(
                actuators_available[idx].action_space, Box
            ), f'{actuators_available[idx].action_space} must be "Box" type'
            value = np.interp(
                action,
                input_range,
                [
                    actuators_available[idx].action_space.low[0],
                    actuators_available[idx].action_space.high[0],
                ],
            )
            if np.isnan(value):
                LOG.critical("Cannot np.interp value " + str(action))
            actuators_available[idx]([value])
            value_list.append(value)
        return actuators_available, value_list

    @staticmethod
    def noise_scaling(in_min, in_max, actuators_available, noises):
        """Method to scale the noises to the given actuator space.

        If the network output space changes, this method needs to be
        modified as well.
        """

        input_range = [in_min, in_max]

        value_list = []

        for idx, noise in enumerate(noises):
            value = np.interp(
                noise,
                input_range,
                [
                    actuators_available[idx].action_space.low[0],
                    actuators_available[idx].action_space.high[0],
                ],
            )
            if np.isnan(value):
                LOG.critical("Cannot np.interp value " + str(noise))
            value_list.append(value)
        return value_list

    @staticmethod
    def clip_actions_box(actions, low, high):
        return np.clip(actions, low, high)

    @staticmethod
    def output_clipping(actuators_available, actions) -> list:
        for idx, action in enumerate(actions):
            assert isinstance(
                actuators_available[idx].action_space, Box
            ), f'{actuators_available[idx].action_space} must be "Box" type'

            low = actuators_available[idx].action_space.low[0]
            high = actuators_available[idx].action_space.high[0]
            value = DDPGMuscle.clip_actions_box(action, low, high)
            actuators_available[idx]([value])
        return actuators_available

    def propose_actions(
            self, sensors, actuators_available, is_terminal=False
    ):
        values = np.concatenate([val() for val in sensors])
        obs = torch.tensor(
            values,
            dtype=torch.float,
        )

        noise = self._mode == Mode.TRAIN

        #        state = obs.to(self.model.device)
        #        mu_prime = self.model.forward(state).to(self.model.device)
        #        if noise is True:
        #            if self.noise is not None:
        #                self.noise.reset()  # TODO: Reset after every episode!
        #            else:
        #                self.noise = OUActionNoise(
        #                    mu=np.zeros(len(actuators_available))
        #                )
        #            mu_prime += torch.tensor(self.noise(), dtype=torch.float).to(
        #                self.model.device
        #            )
        #        actions = self._clip_actions(mu_prime, actuators_available)[0]
        #
        #        assert len(actions) == len(actuators_available)
        #
        #        env_actuators = self.output_scaling(actuators_available, actions)

        state = obs.to(self.model.device)
        actions = self.model.forward(state).to(self.model.device)

        if self.make_noise and noise:
            min_clip_box, max_clip_box = self._create_reset_noise_func(actuators_available)
            noise_val = self.noise()

            noise_val = DDPGMuscle.clip_actions_box(noise_val, min_clip_box, max_clip_box)

            [noise_val] = self.noise_scaling(min_clip_box, max_clip_box, actuators_available, noise_val)
            actions += noise_val

        actions = self._get_action_list(actions)
        env_actuators = self.output_clipping(actuators_available, actions)

        return (env_actuators, actions, values, {})

    def _create_reset_noise_func(self, actuators_available):
        if self.noise_func == "OUActionNoise":
            if self.noise is not None:
                self.noise.reset()  # TODO: Reset after every episode!
            else:
                self.noise = OUActionNoise(
                    mu=np.zeros(len(actuators_available)),
                    sigma=self.sigma, theta=self.theta, dt=self.dt
                )
            min_clip_box, max_clip_box = 0, 1
        elif self.noise_func == "Gaussian":
            if self.noise is None:
                self.noise = GaussianNoise(sigma=self.sigma)
            min_clip_box, max_clip_box = -1, 1
        else:
            LOG.critical("Provided noise function not found")
            raise Exception("Provided noise function not found")
        return min_clip_box, max_clip_box

    def _clip_actions(self, actions, actuators: list):
        """Clip actions to make sure they are not higher than max or lower than min of allowed action space."""
        # TODO: Assumption: sigmoid output
        return np.clip(actions.cpu().detach().numpy(), 0, 1)

    def _get_action_list(self, actions):
        if "cuda" in self.device.type:
            actions_list = actions.cpu()
        else:
            actions_list = actions
        actions_list = actions_list.detach().numpy()[0]
        return actions_list

    def update(self, update: dict):
        """Update weights of actor network."""
        if self.model is None and update is not None:
            self._init(**update)
        elif update is not None:
            self._update_weights(weights=update)
        elif self.model is None and update is None:
            print("ERROR: UPDATE AND MODEL ARE NONE!")

    def _init(self, n_inputs, n_outputs, fc_dims, weights):
        """Initialize the actor-network."""
        self.model = ActorNetwork(
            n_inputs=n_inputs,
            fc_dims=fc_dims,
            n_actions=n_outputs,
            name="Muscle (actor)",
        )
        self._update_weights(weights=weights)
        self.model.eval()

    def _update_weights(self, weights):
        self.model.load_state_dict(weights)

    def prepare_model(self):
        """Prepare the model for training."""
        data = open(self._load_path + "/ddpg_actor", "rb")
        self.model = torch.load(data, map_location=self.device)
        self.model.device = self.device
        self.model.eval()

    def store_model(self, tag: Optional[str] = None):
        path = self._load_path
        torch.save(self.model.nn_model, f"{path}/ddpg_muscle_actor_nn")

    @property
    def parameters(self) -> dict:
        """Return the parameters of the actor-network."""
        return {"Type": type(self.model)}

    def __repr__(self):
        return f"DDPG-Muscle-{self.uid}"

    def current_performance(self) -> float:
        pass


if __name__ == "__main__":
    muscle = DDPGMuscle(broker_uri="test", brain_uri="test", uid=1)

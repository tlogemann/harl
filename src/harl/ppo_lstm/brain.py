"""
Brain for the PPO implementation in palaestrai.
"""

import socket
from pathlib import Path
from typing import List, Union

import numpy as np
import torch as T

from harl.ppo.network import (
    PPOMemory,
    ActorNetwork,
    CriticNetwork,
)
from palaestrai.agent import (
    Brain,
    Objective,
    SensorInformation,
    ActuatorInformation,
    LOG,
)
from palaestrai.core.protocol import MuscleUpdateResponse
from palaestrai.util.exception import NoneInputError


class PPOBrain(Brain):
    def __init__(
        self,
        muscle_updates_listen_uri_or_socket: Union[str, socket.socket],
        sensors: List[SensorInformation],
        actuators: List[ActuatorInformation],
        objective: Objective,
        seed: int,
        **params,
    ):
        super().__init__(
            muscle_updates_listen_uri_or_socket,
            sensors,
            actuators,
            objective,
            seed,
            **params,
        )

        T.manual_seed(self.seed)

        self.timesteps_per_batch = params.get("timesteps_per_batch", 4)
        self.max_timesteps_per_episode = params.get(
            "max_timesteps_per_episode", 96
        )
        self.n_updates_per_iteration = params.get(
            "n_updates_per_iteration", 50
        )
        self.actor_lr = params.get("actor_lr", 0.0003)
        self.critic_lr = params.get("critic_lr", 0.001)
        self.gamma = params.get("gamma", 0.99)
        self.clip = params.get("clip", 0.2)
        self.gae_lambda = params.get("gae_lambda", 0.95)

        self.action_std_init = params.get("action_std_init", 0.6)
        self.action_std_decay_rate = params.get("action_std_decay_rate", 0.05)
        self.min_action_std = params.get("min_action_std", 0.1)
        self.action_std_decay_freq = params.get(
            "action_std_decay_freq", int(2.5e5)
        )

        self.sen_dim = len(self.sensors)
        self.act_dim = len(self.actuators)

        self.actor = ActorNetwork(self.sen_dim, self.act_dim, self.actor_lr)
        self.critic = CriticNetwork(self.sen_dim, self.critic_lr)

        self.step = 0
        self.action_std = self.action_std_init

        self.memory = PPOMemory(self.timesteps_per_batch)
        self.last_actions = None

        self.init_muscle = False
        self.t = 0

    def _remember(self, state, action, probs, vals, reward, done):
        self.memory.store_memory(state, action, probs, vals, reward, done)

    def thinking(
        self,
        muscle_id,
        readings,
        actions,
        reward,
        next_state,
        done,
        additional_data,
    ) -> MuscleUpdateResponse:

        if not self.init_muscle:
            self.init_muscle = True
            return MuscleUpdateResponse(True, self._get_init_dict())

        if (
            readings is None
            or actions is None
            or reward is None
            or done is None
        ):
            LOG.error(
                "Brain(id=0x%x) has received a none value",
                id(self),
            )
            raise NoneInputError

        self._remember(
            readings,
            actions,
            additional_data["probs"],
            additional_data["vals"],
            reward,
            done,
        )
        self.step += 1
        if self.step % self.max_timesteps_per_episode == 0:
            response = self._learn()
        else:
            response = MuscleUpdateResponse(False, None)

        if self.step % self.action_std_decay_freq == 0:
            self.decay_action_std()
        return response

    def _learn(self):
        for _ in range(self.n_updates_per_iteration):
            (
                state_arr,
                action_arr,
                old_prob_arr,
                values,
                reward_arr,
                dones_arr,
                batches,
            ) = self.memory.generate_batches()

            advantage = np.zeros(len(reward_arr), dtype=np.float32)

            for t in range(len(reward_arr) - 1):
                discount = 1
                a_t = 0
                for k in range(t, len(reward_arr) - 1):
                    a_t += discount * (
                        reward_arr[k]
                        + self.gamma * values[k + 1] * (1 - int(dones_arr[k]))
                        - values[k]
                    )
                    discount *= self.gamma * self.gae_lambda
                advantage[t] = a_t
            advantage = T.tensor(advantage).to(self.actor.device)

            values = T.tensor(values).to(self.actor.device)
            for batch in batches:
                states = T.tensor(state_arr[batch], dtype=T.float).to(
                    self.actor.device
                )
                old_probs = T.tensor(old_prob_arr[batch]).to(self.actor.device)
                actions = T.tensor(action_arr[batch]).to(self.actor.device)

                critic_value = self.critic(states)
                critic_value = T.squeeze(critic_value)

                dist = self.actor(states)
                new_probs = dist.log_prob(actions)
                prob_ratio = new_probs.exp() / old_probs.exp()
                weighted_probs = advantage[batch] * prob_ratio
                weighted_clipped_probs = (
                    T.clamp(prob_ratio, 1 - self.clip, 1 + self.clip)
                    * advantage[batch]
                )
                actor_loss = -T.min(
                    weighted_probs, weighted_clipped_probs
                ).mean()

                returns = advantage[batch] + values[batch]
                critic_loss = (returns - critic_value) ** 2
                critic_loss = critic_loss.mean()

                total_loss = actor_loss + 0.5 * critic_loss
                self.actor.optimizer.zero_grad()
                self.critic.optimizer.zero_grad()
                total_loss.backward()
                self.actor.optimizer.step()
                self.critic.optimizer.step()

        self.memory.clear_memory()
        return MuscleUpdateResponse(
            True,
            {
                "actor": self._get_actor_params(),
                "critic": self._get_critic_params(),
                "var": self.actor.action_var,
            },
        )

    def load_model(self, path):
        self.critic = T.load(f"{path}/ppo_critic")
        self.actor = T.load(f"{path}/ppo_actor")

    def store_model(self, path):
        self.store_path = path
        self._store_model_to_subfolder(path)

    def _store_model_to_subfolder(self, path):
        try:
            Path(path).mkdir(parents=True, exist_ok=True)
        except OSError as e:
            LOG.warning(
                "Brain(id=0x%x) failed to create path to save model %s",
                id(self),
                str(e),
            )
            return

        T.save(self.critic, f"{path}/ppo_critic")
        T.save(self.actor, f"{path}/ppo_actor")

    def _get_init_dict(self):
        """Special dict for the initialzation of the muscle."""
        return {
            "weights": {
                "actor": self._get_actor_params(),
                "critic": self._get_critic_params(),
            },
            "in_dim": len(self.sensors),
            "out_dim": len(self.actuators),
        }

    def _get_actor_params(self):
        return dict(self.actor.named_parameters())

    def _get_critic_params(self):
        return dict(self.critic.named_parameters())

    def set_action_std(self, new_action_std):
        self.action_std = new_action_std
        self.actor.set_action_std(new_action_std)

    def decay_action_std(self):
        self.action_std = self.action_std - self.action_std_decay_rate
        self.action_std = round(self.action_std, 4)
        if self.action_std <= self.min_action_std:
            self.action_std = self.min_action_std
        self.set_action_std(self.action_std)

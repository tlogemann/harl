"""
    This file contains a neural network module for us to
    define our actor and critic networks in PPO.
"""

import torch
from torch import nn
import torch.nn.functional as F
import numpy as np
from torch.distributions import MultivariateNormal
from torch.optim import Adam


class ActorNetwork(nn.Module):
    def __init__(
        self,
        state_dim,
        action_dim,
        lr=0.003,
        fc1_dims=256,
        fc2_dims=256,
        action_std_init=0.6,
    ):
        super(ActorNetwork, self).__init__()
        self.action_dim = action_dim
        self.action_var = torch.full(
            (action_dim,), action_std_init * action_std_init
        )

        self.actor = nn.Sequential(
            nn.LSTM(state_dim, fc1_dims),
            nn.ReLU(),
            nn.LSTM(fc1_dims, fc2_dims),
            nn.ReLU(),
            nn.LSTM(fc2_dims, action_dim),
            nn.Tanh(),
        )

        self.optimizer = Adam(self.actor.parameters(), lr=lr)
        self.device = torch.device(
            "cuda:0" if torch.cuda.is_available() else "cpu"
        )
        self.to(self.device)

    def forward(self, state):
        action_mean = self.actor(state.to(self.device))
        cov_mat = torch.diag(self.action_var).unsqueeze(dim=0).to(self.device)
        dist = MultivariateNormal(action_mean, cov_mat)

        return dist

    def set_action_std(self, new_action_std):
        self.action_var = torch.full(
            (self.action_dim,), new_action_std * new_action_std
        ).to(self.device)


class CriticNetwork(nn.Module):
    def __init__(self, state_dim, lr=0.001, fc1_dims=256, fc2_dims=256):
        super(CriticNetwork, self).__init__()

        self.critic = nn.Sequential(
            nn.Linear(state_dim, fc1_dims),
            nn.ReLU(),
            nn.Linear(fc1_dims, fc2_dims),
            nn.ReLU(),
            nn.Linear(fc2_dims, 1),
        )

        self.optimizer = Adam(self.critic.parameters(), lr=lr)
        self.device = torch.device(
            "cuda:0" if torch.cuda.is_available() else "cpu"
        )
        self.to(self.device)

    def forward(self, state):
        value = self.critic(state.to(self.device))

        return value


class PPOMemory:
    def __init__(self, batch_size):
        self.states = []
        self.probs = []
        self.vals = []
        self.actions = []
        self.rewards = []
        self.dones = []

        self.batch_size = batch_size

    def generate_batches(self):
        n_states = len(self.states)
        batch_start = np.arange(0, n_states, self.batch_size)
        indices = np.arange(n_states, dtype=np.int64)
        np.random.shuffle(indices)
        batches = [indices[i : i + self.batch_size] for i in batch_start]

        return (
            np.array(self.states),
            np.array(self.actions),
            np.array(self.probs),
            np.array(self.vals),
            np.array(self.rewards),
            np.array(self.dones),
            batches,
        )

    def store_memory(self, state, action, probs, vals, reward, done):
        self.states.append(state)
        self.actions.append(action)
        self.probs.append(probs)
        self.vals.append(vals)
        self.rewards.append(reward)
        self.dones.append(done)

    def clear_memory(self):
        self.states = []
        self.probs = []
        self.actions = []
        self.rewards = []
        self.dones = []
        self.vals = []

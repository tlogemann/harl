import numpy as np
import torch

from harl.TD3.model import Actor
from palaestrai.agent import Muscle
from palaestrai.types import Box


class TD3Muscle(Muscle):
    def __init__(self, broker_uri, brain_uri, uid, brain_id):
        super().__init__(broker_uri, brain_uri, uid, brain_id)
        self.model = None

    def setup(self):
        pass

    def output_scaling(self, actuators_available, actions):
        """Method to scale the output to the given actuator space.

        If the network output space changes, this method needs to be
        modified as well.
        """
        assert len(actions) == len(actuators_available)

        input_range = [-1, 1]

        for idx, action in enumerate(actions):
            assert isinstance(
                actuators_available[idx].action_space, Box
            ), f'{actuators_available[idx].action_space} must be "Box" type'
            value = np.interp(
                action,
                input_range,
                [
                    actuators_available[idx].action_space.low[0],
                    actuators_available[idx].action_space.high[0],
                ],
            )
            actuators_available[idx]([value])
        return actuators_available

    def propose_actions(
        self, sensors, actuators_available, is_terminal=False
    ) -> list:
        values = np.concatenate([val() for val in sensors])
        obs = torch.tensor(
            values,
            dtype=torch.float,
        )
        state = obs.to(torch.device("cpu"))
        actions = self.model(state).cpu().data.numpy().flatten()

        assert len(actions) == len(actuators_available)

        env_actuators = self.output_scaling(actuators_available, actions)

        return [env_actuators, actions, values, {}]

    def update(self, update: dict):
        if self.model is None and update is not None:
            self._init(**update)
        elif update is not None:
            self._update_weights(weights=update)
        elif self.model is None and update is None:
            print("ERROR: UPDATE AND MODEL ARE NONE!")

    def _init(self, n_inputs, n_outputs, weights):
        """Initialize the actor-network."""
        self.model = Actor(
            state_dim=n_inputs,
            action_dim=n_outputs,
        )
        self._update_weights(weights=weights)

    def _update_weights(self, weights):
        self.model.load_state_dict(weights)

    def prepare_model(self):
        data = open(self._load_path + "/td3_actor", "rb")
        self.model = torch.load(data).to("cpu")

    def __repr__(self):
        pass

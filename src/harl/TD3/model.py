from typing import List

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from palaestrai.agent import (
    SensorInformation,
    ActuatorInformation,
    RewardInformation,
)


class Actor(nn.Module):
    def __init__(self, state_dim, action_dim):
        super(Actor, self).__init__()
        self.state_dim = state_dim

        self.l1 = nn.Linear(state_dim, 256)
        self.l2 = nn.Linear(256, 256)
        self.l3 = nn.Linear(256, action_dim)

    def forward(self, state):
        state = state.view(-1, self.state_dim)
        a = F.relu(self.l1(state))
        a = F.relu(self.l2(a))
        return torch.tanh(self.l3(a))


class Critic(nn.Module):
    def __init__(self, state_dim, action_dim):
        super(Critic, self).__init__()

        # Q1 architecture
        self.l1 = nn.Linear(state_dim + action_dim, 256)
        self.l2 = nn.Linear(256, 256)
        self.l3 = nn.Linear(256, 1)

        # Q2 architecture
        self.l4 = nn.Linear(state_dim + action_dim, 256)
        self.l5 = nn.Linear(256, 256)
        self.l6 = nn.Linear(256, 1)

    def forward(self, state, action):
        sa = torch.cat([state, action], 1)

        q1 = F.relu(self.l1(sa))
        q1 = F.relu(self.l2(q1))
        q1 = self.l3(q1)

        q2 = F.relu(self.l4(sa))
        q2 = F.relu(self.l5(q2))
        q2 = self.l6(q2)
        return q1, q2

    def Q1(self, state, action):
        sa = torch.cat([state, action], 1)

        q1 = F.relu(self.l1(sa))
        q1 = F.relu(self.l2(q1))
        q1 = self.l3(q1)
        return q1


class ReplayBuffer(object):
    def __init__(self, state_dim, action_dim, max_size=int(1e6)):
        self.max_size = max_size
        self.ptr = 0
        self.size = 0

        self.state_memory = np.zeros((self.max_size, state_dim))
        self.new_state_memory = np.zeros((self.max_size, state_dim))
        self.action_memory = np.zeros((self.max_size, action_dim))
        self.reward_memory = np.zeros(self.max_size)
        self.terminal_memory = np.zeros(self.max_size, dtype=np.float32)

        self.device = torch.device(
            "cuda" if torch.cuda.is_available() else "cpu"
        )

    def add(self, state, action, next_state, reward, done):
        self.state_memory[self.ptr] = state
        self.action_memory[self.ptr] = action
        self.new_state_memory[self.ptr] = next_state
        self.reward_memory[self.ptr] = reward
        self.terminal_memory[self.ptr] = 1 if done else 0

        self.ptr = (self.ptr + 1) % self.max_size
        self.size = min(self.size + 1, self.max_size)

    def sample(self, batch_size):
        ind = np.random.randint(0, self.size, size=batch_size)

        return (
            torch.FloatTensor(self.state_memory[ind]).to(self.device),
            torch.FloatTensor(self.action_memory[ind]).to(self.device),
            torch.FloatTensor(self.new_state_memory[ind]).to(self.device),
            torch.FloatTensor(self.reward_memory[ind]).to(self.device),
            torch.FloatTensor(self.terminal_memory[ind]).to(self.device),
        )

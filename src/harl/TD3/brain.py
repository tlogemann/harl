import copy
import socket
import uuid
from pathlib import Path
from typing import List, Union

import numpy as np
import torch
import torch.nn.functional as F

from harl.TD3.model import Actor, Critic, ReplayBuffer
from palaestrai.agent import (
    Brain,
    SensorInformation,
    ActuatorInformation,
    Objective,
    LOG,
)
from palaestrai.core.protocol import MuscleUpdateResponse
from palaestrai.util.exception import NoneInputError


class TD3Brain(Brain):
    def __init__(
        self,
        muscle_updates_listen_uri_or_socket: Union[str, socket.socket],
        sensors: List[SensorInformation],
        actuators: List[ActuatorInformation],
        objective: Objective,
        seed: int,
        **params,
    ):
        super().__init__(
            muscle_updates_listen_uri_or_socket,
            sensors,
            actuators,
            objective,
            seed,
            **params,
        )

        assert len(sensors) != 0
        assert len(actuators) != 0

        self.init_muscle = False
        torch.manual_seed(seed)
        self.n_sensors = len(sensors)
        self.n_actuators = len(actuators)
        self.log_reward = []  # TODO: delete
        self.avrg_reward = []
        self.critic_error = []
        self.avrg_critic_error = []
        self.actor_error = []
        self.avrg_actor_error = []
        self.id = uuid.uuid4()

        self.discount = params.get("discount", 0.99)
        self.tau = params.get("tau", 0.005)
        self.policy_noise = params.get("policy_noise", 0.2)
        self.noise_clip = params.get("noise_clip", 0.5)
        self.policy_freq = params.get("policy_freq", 2)
        self.batch_size = params.get("batch_size", 128)
        self.actor_lr = params.get("actor_lr", 3e-4)
        self.critic_lr = params.get("critic_lr", 3e-4)

        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

        self.actor = Actor(self.n_sensors, self.n_actuators).to(device)
        self.actor_target = copy.deepcopy(self.actor)
        self.actor_optimizer = torch.optim.Adam(
            self.actor.parameters(), lr=self.actor_lr
        )

        self.critic = Critic(self.n_sensors, self.n_actuators).to(device)
        self.critic_target = copy.deepcopy(self.critic)
        self.critic_optimizer = torch.optim.Adam(
            self.critic.parameters(), lr=self.critic_lr
        )

        self.buffer = ReplayBuffer(self.n_sensors, self.n_actuators)

        self.total_it = 0

    def _remember(self, state, action, reward, next_state, done):
        self.buffer.add(
            state=np.array(state),
            action=np.array(action),
            reward=reward,
            next_state=np.concatenate([s() for s in next_state]),
            done=done,
        )

    def _get_actor_params(self):
        return dict(self.actor.named_parameters())

    def thinking(
        self,
        muscle_id,
        readings,
        actions,
        reward,
        next_state,
        done,
        additional_data,
    ) -> MuscleUpdateResponse:
        # TODO: How to deal with self.last_state when env is done?
        self.log_reward.append(reward)
        self.avrg_reward.append(np.mean(self.log_reward[-300:]))
        self.avrg_critic_error.append(np.mean(self.critic_error[-300:]))
        self.avrg_actor_error.append(np.mean(self.actor_error[-300:]))

        if not self.init_muscle:
            self.init_muscle = True
            response = MuscleUpdateResponse(True, self._get_init_dict())
        else:
            if done:
                response = MuscleUpdateResponse(False, None)
            else:
                if (
                    readings is None
                    or actions is None
                    or reward is None
                    or not next_state
                    or done is None
                ):
                    LOG.error(
                        "Brain(id=0x%x) " "has received a none value",
                        id(self),
                    )
                    raise NoneInputError
                else:
                    self._remember(readings, actions, reward, next_state, done)
                    # Currently training and updating every step!
                    response = self._train()

        return response

    def _train(self):
        self.total_it += 1
        state, action, next_state, reward, not_done = self.buffer.sample(
            self.batch_size
        )

        with torch.no_grad():
            # Select action according to policy and add clipped noise
            noise = (torch.randn_like(action) * self.policy_noise).clamp(
                -self.noise_clip, self.noise_clip
            )

            next_action = (self.actor_target(next_state) + noise).clamp(-1, 1)

            # Compute the target Q value
            target_Q1, target_Q2 = self.critic_target(next_state, next_action)
            target_Q = torch.min(target_Q1.flatten(), target_Q2.flatten())
            target_Q = reward + not_done * self.discount * target_Q

        # Get current Q estimates
        current_Q1, current_Q2 = self.critic(state, action)

        # Compute critic loss
        critic_loss = F.mse_loss(current_Q1.flatten(), target_Q) + F.mse_loss(
            current_Q2.flatten(), target_Q
        )

        # Optimize the critic
        self.critic_optimizer.zero_grad()
        critic_loss.backward()
        self.critic_optimizer.step()

        # Delayed policy updates
        if self.total_it % self.policy_freq == 0:

            # Compute actor losse
            actor_loss = -self.critic.Q1(state, self.actor(state)).mean()

            # Optimize the actor
            self.actor_optimizer.zero_grad()
            actor_loss.backward()
            self.actor_optimizer.step()

            # Update the frozen target models
            for param, target_param in zip(
                self.critic.parameters(), self.critic_target.parameters()
            ):
                target_param.data.copy_(
                    self.tau * param.data + (1 - self.tau) * target_param.data
                )

            for param, target_param in zip(
                self.actor.parameters(), self.actor_target.parameters()
            ):
                target_param.data.copy_(
                    self.tau * param.data + (1 - self.tau) * target_param.data
                )

        return MuscleUpdateResponse(True, self._get_actor_params())

    def _get_init_dict(self):
        """Special dict for the initialzation of the muscle."""
        return {
            "weights": self._get_actor_params(),
            "n_inputs": self.n_sensors,
            "n_outputs": self.n_actuators,
        }

    def store_model(self, path, **kwargs):
        self.store_path = path
        self._store_model_to_subfolder(path)

    def _store_model_to_subfolder(self, path):
        try:
            Path(path).mkdir(parents=True, exist_ok=True)
        except OSError as e:
            LOG.warning(
                "Brain(id=0x%x) failed to create path to save model %s",
                id(self),
                str(e),
            )
            return

        torch.save(self.critic, f"{path}/td3_critic")
        torch.save(self.actor, f"{path}/td3_actor")

        torch.save(self.critic_target, f"{path}/td3_critic_target")
        torch.save(self.actor_target, f"{path}/td3_actor_target")

    def load_model(self, path, **kwargs):
        self.critic = torch.load(f"{path}/td3_critic")
        self.actor = torch.load(f"{path}/td3_actor")
        self.critic_target = torch.load(f"{path}/td3_critic_target")
        self.actor_target = torch.load(f"{path}/td3_actor_target")

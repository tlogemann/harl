# dqn-rainbow implementation in palaestrai's brain-muscle scheme. Based on https://github.com/Kaixhin/Rainbow
import logging
import socket
from pathlib import Path
from typing import Union, List

import numpy as np
import torch as T
from torch import optim

from palaestrai.agent import ActuatorInformation, SensorInformation, Objective
from palaestrai.agent.brain import Brain
from palaestrai.core.protocol import MuscleUpdateResponse
from .buffer import ReplayMemory
from .model import DQN

LOG = logging.getLogger(__name__)


class DQNBrain(Brain):
    def __init__(
        self,
        muscle_updates_listen_uri_or_socket: Union[str, socket.socket],
        sensors: List[SensorInformation],
        actuators: List[ActuatorInformation],
        objective: Objective,
        seed: int,
        **params,
    ):
        super().__init__(
            muscle_updates_listen_uri_or_socket,
            sensors,
            actuators,
            objective,
            seed,
            **params,
        )
        self._params = params
        T.manual_seed(seed)
        self.steps = 0
        self.init_muscles = []

        self._first_readings = None
        self._first_actions = None

        self.action_space = sum([len(a) for a in actuators])
        self.input_space = sum([len(s) for s in sensors])
        self.device = params.get(
            "device", "cuda:0" if T.cuda.is_available() else "cpu"
        )

        # the defaut values are taken from the rainbow paper (https://arxiv.org/pdf/1710.02298.pdf)
        self.atoms = params.get("atoms", 51)
        self.Vmin = params.get("vmin", -10)
        self.Vmax = params.get("vmax", 10)
        self.batch_size = params.get("batch_size", 32)
        self.n = params.get("multi_step", None)
        if not self.n:
            self.n = params.get("n", 3)
        self.discount = params.get("discount", 0.99)
        self.norm_clip = params.get("norm_clip", 10)
        self.learning_rate = params.get("learning_rate", None)
        if not self.learning_rate:
            self.learning_rate = params.get("lr", 0.0000625)
        self.adam_eps = params.get("adam_eps", 0.00015)
        self.min_replay_size = params.get("min_replay_size", 1_000_000)
        self.replay_frequency = params.get("replay_frequency", 4)
        self.priority_weight = params.get("priority_weight", 0.4)
        self.expected_steps = params.get("expected_steps", 50e6)
        self.target_net_update = params.get("target_update", 10_000)

        self._memory = None
        self._support = None

        assert self.atoms > 1, "the atoms parameter must be at least 2"
        self.delta_z = (self.Vmax - self.Vmin) / (self.atoms - 1)

        self.priority_weight_increase = (1 - self.priority_weight) / (
            self.expected_steps - self.min_replay_size
        )

        self._online_net = None

        self._target_net = None
        for param in self.target_net.parameters():
            param.requires_grad = False

        self._optimiser = None

    # we define some variables as properties, because we can onl call .device after the brain was spawned in a
    # different process
    @property
    def memory(self):
        if self._memory is None:
            self._memory = ReplayMemory(
                self._params,
                self._first_readings.numpy(),
                np.array(self._first_actions),
            )
        return self._memory

    @property
    def support(self):
        if self._support is None:
            self._support = T.linspace(self.Vmin, self.Vmax, self.atoms).to(
                device=self.device
            )
        return self._support

    @property
    def online_net(self):
        if self._online_net is None:
            self._online_net = DQN(
                self._params, self.action_space, self.input_space, self.support
            ).to(device=self.device)
            self._online_net.train()
        return self._online_net

    @property
    def target_net(self):
        if self._target_net is None:
            self._target_net = DQN(
                self._params, self.action_space, self.input_space, self.support
            ).to(device=self.device)
            self.update_target_net()
            self.target_net.train()
        return self._target_net

    @property
    def optimiser(self):
        if self._optimiser is None:
            self._optimiser = optim.Adam(
                self.online_net.parameters(),
                lr=self.learning_rate,
                eps=self.adam_eps,
            )
        return self._optimiser

    def thinking(
        self,
        muscle_id,
        readings,
        actions,
        reward,
        next_state,
        done,
        additional_data,
    ) -> MuscleUpdateResponse:
        if readings is None or muscle_id not in self.init_muscles:
            self.init_muscles.append(muscle_id)
            return MuscleUpdateResponse(True, self._get_init_dict())

        if self._first_readings is None:
            self._first_readings = readings

        if self._first_actions is None:
            self._first_actions = actions

        next_state = [s() for s in next_state]

        # save new states
        self.memory.append(readings, actions, reward, done, next_state)
        self.steps += 1  # TODO: we may want to handle this independently per registered muscle

        if len(self.memory) >= self.min_replay_size:
            self.memory.priority_weight = min(
                self.memory.priority_weight + self.priority_weight_increase, 1
            )

            if self.steps % self.target_net_update:
                self.update_target_net()

            if self.steps % self.replay_frequency == 0:
                # sample Replay buffer
                (
                    idxs,
                    states,
                    actions,
                    returns,
                    next_states,
                    nonterminals,
                    weights,
                ) = self.memory.sample(self.batch_size)
                # Calculate current state probabilities (online network noise already sampled)
                log_ps = self.online_net(
                    states, log=True
                )  # Log probabilities log p(s_t, ·; θonline)
                log_ps_a = log_ps[
                    range(self.batch_size), actions
                ]  # log p(s_t, a_t; θonline)
                with T.no_grad():
                    # Calculate nth next state probabilities
                    pns = self.online_net(
                        next_states
                    )  # Probabilities p(s_t+n, ·; θonline)
                    dns = (
                        self.support.expand_as(pns) * pns
                    )  # Distribution d_t+n = (z, p(s_t+n, ·; θonline))
                    argmax_indices_ns = dns.sum(2).argmax(
                        1
                    )  # Perform argmax action selection using online network: argmax_a[(z, p(s_t+n, a; θonline))]
                    self.target_net.reset_noise()  # Sample new target net noise
                    pns = self.target_net(
                        next_states
                    )  # Probabilities p(s_t+n, ·; θtarget)
                    pns_a = pns[
                        range(self.batch_size), argmax_indices_ns
                    ]  # Double-Q probabilities p(s_t+n, argmax_a[(z, p(s_t+n, a; θonline))]; θtarget))
                    # Compute Tz (Bellman operator T applied to z)
                    Tz = returns.unsqueeze(1) + nonterminals * (
                        self.discount ** self.n
                    ) * self.support.unsqueeze(
                        0
                    )  # Tz = R^n + (γ^n)z (accounting for terminal states)
                    Tz = Tz.clamp(
                        min=self.Vmin, max=self.Vmax
                    )  # Clamp between supported values
                    # Compute L2 projection of Tz onto fixed support z
                    b = (Tz - self.Vmin) / self.delta_z  # b = (Tz - Vmin) / Δz
                    l, u = b.floor().to(T.int64), b.ceil().to(T.int64)
                    # Fix disappearing probability mass when l = b = u (b is int)
                    l[(u > 0) * (l == u)] -= 1
                    u[(l < (self.atoms - 1)) * (l == u)] += 1
                    # Distribute probability of Tz
                    m = states.new_zeros(self.batch_size, self.atoms)
                    offset = (
                        T.linspace(
                            0,
                            ((self.batch_size - 1) * self.atoms),
                            self.batch_size,
                        )
                        .unsqueeze(1)
                        .expand(self.batch_size, self.atoms)
                        .to(actions)
                    )
                    m.view(-1).index_add_(
                        0,
                        (l + offset).view(-1),
                        (pns_a * (u.float() - b)).view(-1),
                    )  # m_l = m_l + p(s_t+n, a*)(u - b)
                    m.view(-1).index_add_(
                        0,
                        (u + offset).view(-1),
                        (pns_a * (b - l.float())).view(-1),
                    )  # m_u = m_u + p(s_t+n, a*)(b - l)
                loss = -T.sum(
                    T.sum(m * log_ps_a, -1), -1
                )  # Cross-entropy loss (minimises DKL(m||p(s_t, a_t)))
                self.online_net.zero_grad()
                (
                    weights * loss
                ).mean().backward()  # Backpropagate importance-weighted minibatch loss
                T.nn.utils.clip_grad_norm_(
                    self.online_net.parameters(), self.norm_clip
                )  # Clip gradients by L2 norm
                self.optimiser.step()
                self.memory.update_priorities(
                    idxs, loss.detach().cpu().numpy()
                )  # Update priorities of sampled transitions
                if self.steps % self.replay_frequency == 0:
                    self.online_net.reset_noise()
                return MuscleUpdateResponse(True, self.online_net.state_dict())

        if self.steps % self.replay_frequency == 0:
            self.online_net.reset_noise()
            return MuscleUpdateResponse(True, self.online_net.state_dict())

        return MuscleUpdateResponse(False, None)

    def store_model(self, path):
        try:
            Path(path).mkdir(parents=True, exist_ok=True)
        except OSError as e:
            LOG.warning(
                "Brain(id=0x%x) failed to create path to save model %s",
                id(self),
                str(e),
            )
            return

        T.save(self.online_net, f"{path}/dqn_online")
        T.save(self.target_net, f"{path}/dqn_target")

    def load_model(self, path):
        self._online_net = T.load(
            f"{path}/dqn_online", map_location=self.device
        )
        self._target_net = T.load(
            f"{path}/dqn_target", map_location=self.device
        )
        self._support = self.online_net.support

    def update_target_net(self):
        self.target_net.load_state_dict(self.online_net.state_dict())

    def reset_noise(self):
        self.online_net.reset_noise()

    def _get_init_dict(self):
        """Special dict for the initialization of the muscle."""
        return {
            "atoms": self.online_net.atoms,
            "layers": self.online_net.layers,
            "kernel_size": self.online_net.kernel_size,
            "architecture": self.online_net.architecture,
            "strides": self.online_net.strides,
            "paddings": self.online_net.paddings,
            "noisy_std": self.online_net.noisy_std,
            "history_length": self.online_net.history_length,
            "hidden_size": self.online_net.hidden_size,
            "action_space": self.online_net.action_space,
            "input_space": self.online_net.input_space,
            "weights": self.online_net.state_dict(),
            "support": self.support,
        }

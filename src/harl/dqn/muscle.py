import numpy.random
import torch as T
import torch.nn.functional as F
from numpy.random import default_rng
from palaestrai.agent.muscle import Muscle

from .model import DQN


def get_actions(actuators, q, support, rng=None):
    index = 0
    for a in actuators:
        if rng is None:
            a((q[index : index + len(a)] * support).sum(2).argmax(1).item())
        else:
            with T.no_grad():
                w = (q[index : index + len(a)] * support).sum(2).flatten()
                soft = F.softmax(w, dim=-1)
                soft = [
                    round(s.item(), 5) for s in soft
                ]  # round to avoid numerical instabilities
                soft[-1] = 1 - sum(
                    soft[0:-1]
                )  # make sure that probabilities sum to 1 after rounding
                a(rng.choice(range(len(a)), p=soft))
        index += a.action_space.n
    return actuators


class DQNMuscle(Muscle):
    def __init__(self, broker_uri, brain_uri, uid, brain_id, **params):
        super().__init__(broker_uri, brain_uri, uid, brain_id)
        self.rng = default_rng(params.get("seed", numpy.random.randint(10000)))
        self.model = None
        self.device = params.get(
            "device", "cuda:0" if T.cuda.is_available() else "cpu"
        )
        self._support = None

    def setup(self, *args, **kwargs):
        pass

    def propose_actions(
        self, sensors, actuators_available, is_terminal=False
    ) -> tuple:
        if not self.model:
            return [], [], [], None

        obs = T.tensor(
            [val() for val in sensors],
            dtype=T.float,
        )

        with T.no_grad():
            state = obs.to(self.device)
            q = self.model.forward(state).to(self.device)
            actions = get_actions(
                actuators_available, q, self.support, self.rng
            )

        return actions, [a.setpoint for a in actions], obs, None

    def update(self, update):
        if self.model is None and update is not None:
            self.model = DQN(
                update,
                update["action_space"],
                update["input_space"],
                update["support"],
            ).to(self.device)
            self._update_weights(weights=update["weights"])
            self._support = update["support"]
        elif update is not None:
            self._update_weights(weights=update)

    def _update_weights(self, weights):
        self.model.load_state_dict(weights)

    def prepare_model(self):
        self.model = T.load(
            f"{self._load_path}/dqn_online", map_location=self.device
        )
        self._support = self.model.support

    @property
    def support(self):
        return self._support

    def __repr__(self):
        return f"DQN-Muscle-{self.uid}"


class DQNEpsilonMuscle(DQNMuscle):
    def __init__(self, broker_uri, brain_uri, uid, brain_id, **params):
        super().__init__(broker_uri, brain_uri, uid, brain_id)
        self.epsilon = params.get("epsilon", None)
        if not self.epsilon:
            self.epsilon = params.get("eps", 0.3)
        self.epsilon_decay = params.get("epsilon_decay", None)
        if not self.epsilon_decay:
            self.epsilon_decay = params.get("eps_decay", 5_000_000)
        self.epsilon_decay = float(self.epsilon) / self.epsilon_decay

    def propose_actions(
        self, sensors, actuators_available, is_terminal=False
    ) -> tuple:
        if not self.model:
            return [], [], [], None

        obs = T.tensor(
            [val() for val in sensors],
            dtype=T.float,
        )

        self.epsilon = max(self.epsilon - self.epsilon_decay, 0)

        if self.epsilon > 0 and self.rng.random() < self.epsilon:
            for a in actuators_available:
                a(a.action_space.sample())
            return (
                actuators_available,
                [a.setpoint for a in actuators_available],
                obs,
                None,
            )

        with T.no_grad():
            state = obs.to(self.device)
            q = self.model.forward(state).to(self.device)
            actions = get_actions(actuators_available, q, self.support)

        return actions, [a.setpoint for a in actions], obs, None


class DQNGreedyMuscle(DQNMuscle):
    def propose_actions(
        self, sensors, actuators_available, is_terminal=False
    ) -> tuple:
        if not self.model:
            return [], [], [], None

        obs = T.tensor(
            [val() for val in sensors],
            dtype=T.float,
        )

        with T.no_grad():
            state = obs.to(self.device)
            q = self.model.forward(state).to(self.device)
            actions = get_actions(actuators_available, q, self.support)

        return actions, [a.setpoint for a in actions], obs, None

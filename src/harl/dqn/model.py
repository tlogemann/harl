# from https://github.com/Kaixhin/Rainbow/blob/master/model.py
from __future__ import division

import numpy as np
import math
import torch
from torch import nn
from torch.nn import functional as F


# Factorised NoisyLinear layer with bias
class NoisyLinear(nn.Module):
    def __init__(self, in_features, out_features, std_init=0.5):
        super(NoisyLinear, self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.std_init = std_init
        self.weight_mu = nn.Parameter(torch.empty(out_features, in_features))
        self.weight_sigma = nn.Parameter(
            torch.empty(out_features, in_features)
        )
        self.register_buffer(
            "weight_epsilon", torch.empty(out_features, in_features)
        )
        self.bias_mu = nn.Parameter(torch.empty(out_features))
        self.bias_sigma = nn.Parameter(torch.empty(out_features))
        self.register_buffer("bias_epsilon", torch.empty(out_features))
        self.reset_parameters()
        self.reset_noise()

    def reset_parameters(self):
        mu_range = 1 / math.sqrt(self.in_features)
        self.weight_mu.data.uniform_(-mu_range, mu_range)
        self.weight_sigma.data.fill_(
            self.std_init / math.sqrt(self.in_features)
        )
        self.bias_mu.data.uniform_(-mu_range, mu_range)
        self.bias_sigma.data.fill_(
            self.std_init / math.sqrt(self.out_features)
        )

    def _scale_noise(self, size):
        x = torch.randn(size, device=self.weight_mu.device)
        return x.sign().mul_(x.abs().sqrt_())

    def reset_noise(self):
        epsilon_in = self._scale_noise(self.in_features)
        epsilon_out = self._scale_noise(self.out_features)
        self.weight_epsilon.copy_(epsilon_out.ger(epsilon_in))
        self.bias_epsilon.copy_(epsilon_out)

    def forward(self, input):
        if self.training:
            return F.linear(
                input,
                self.weight_mu + self.weight_sigma * self.weight_epsilon,
                self.bias_mu + self.bias_sigma * self.bias_epsilon,
            )
        else:
            return F.linear(input, self.weight_mu, self.bias_mu)


class DQN(nn.Module):
    def __init__(self, args, action_space, input_space, support):
        super(DQN, self).__init__()
        self.action_space = action_space
        self.input_space = input_space
        self.atoms = args.get("atoms", 51)
        self.noisy_std = args.get("noisy_std", 0.1)
        self.history_length = args.get("history_length", 1)
        self.hidden_size = args.get("hidden_size", 512)
        self.architecture = args.get("architecture", "none")
        self.layers = args.get("layers", [32, 64, 64])
        self.kernel_size = args.get("kernel_size", [8, 4, 3])
        self.strides = args.get("strides", [4, 2, 1])
        self.paddings = args.get("paddings", [0, 0, 0])
        self.support = (
            support  # we need this parameter after loading the model
        )

        if self.architecture == "canonical":
            self.convs = nn.Sequential(
                nn.Conv2d(self.history_length, 32, 8, stride=4, padding=0),
                nn.ReLU(),
                nn.Conv2d(32, 64, 4, stride=2, padding=0),
                nn.ReLU(),
                nn.Conv2d(64, 64, 3, stride=1, padding=0),
                nn.ReLU(),
            )
            self.conv_output_size = 3136
        elif self.architecture == "data-efficient":
            self.convs = nn.Sequential(
                nn.Conv2d(self.history_length, 32, 5, stride=5, padding=0),
                nn.ReLU(),
                nn.Conv2d(32, 64, 5, stride=5, padding=0),
                nn.ReLU(),
            )
            self.conv_output_size = 576
        elif self.architecture == "custom":
            conv_layers = list(
                np.array(
                    [
                        [
                            nn.Conv2d(
                                self.history_length,
                                self.layers[i],
                                self.kernel_size[i],
                                stride=self.strides[i],
                                padding=self.paddings[i],
                            ),
                            nn.ReLU(),
                        ]
                        for i in range(len(self.layers))
                    ]
                ).flatten()
            )
            self.convs = nn.Sequential(*conv_layers)
            self.conv_output_size = 3136  # TODO: how do we calculate this? maybe: https://stackoverflow.com/a/59130038
        else:
            self.convs = nn.Sequential()
            self.conv_output_size = input_space
        self.fc_h_v = NoisyLinear(
            self.conv_output_size, self.hidden_size, std_init=self.noisy_std
        )
        self.fc_h_a = NoisyLinear(
            self.conv_output_size, self.hidden_size, std_init=self.noisy_std
        )
        self.fc_z_v = NoisyLinear(
            self.hidden_size, self.atoms, std_init=self.noisy_std
        )
        self.fc_z_a = NoisyLinear(
            self.hidden_size,
            action_space * self.atoms,
            std_init=self.noisy_std,
        )

    def forward(self, x, log=False):
        x = self.convs(x)
        x = x.view(-1, self.conv_output_size)
        v = self.fc_z_v(F.relu(self.fc_h_v(x)))  # Value stream
        a = self.fc_z_a(F.relu(self.fc_h_a(x)))  # Advantage stream
        v, a = v.view(-1, 1, self.atoms), a.view(
            -1, self.action_space, self.atoms
        )
        q = v + a - a.mean(1, keepdim=True)  # Combine streams
        if log:  # Use log softmax for numerical stability
            q = F.log_softmax(
                q, dim=2
            )  # Log probabilities with action over second dimension
        else:
            q = F.softmax(
                q, dim=2
            )  # Probabilities with action over second dimension
        return q

    def reset_noise(self):
        for name, module in self.named_children():
            if "fc" in name:
                module.reset_noise()
